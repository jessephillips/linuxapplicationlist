These items are under evaluation to determine if they are ready to become
part of my primary system setup.

# Core System

X Windowing - Wayland (Weston) - https://github.com/wayland-project/weston

Window Manager - https://swaywm.org/

Shell - https://fishshell.com/

# Editing

Text - https://www.onivim.io/