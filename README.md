# LinuxApplicationList

Just a Place I can document the Linux programs I utilize.

I'm running into an issue where there are certain programs I generally have on
on my home system, but if I'm setting up a new system or doing other reconfiguration
I find that I don't actually remember the names of some of my applications.

For example - Firefox, iceweasel: these are easy to remember due to the time I've
used them and the value to get them installed on the machine.

But what about the terminal emulator I use?