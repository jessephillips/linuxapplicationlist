As a Dvork and Vim user I also need to make some key binding adjustments.

Please note that these are just cut and past from different section of the config
You will need to find the section and modify the commands.


```
bindsym $mod+d focus left
bindsym $mod+h focus down
bindsym $mod+t focus up
bindsym $mod+n focus right

# split in horizontal orientation
bindsym $mod+j split h

# split in vertical orientation
bindsym $mod+v split v

# reload the configuration file
bindsym $mod+Shift+r reload

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"


```

This shifts the movement keys to match Vim. And I also apply to moving between
Moving windows and containers, and resizing the window.



-----

Bind Configuration to specific monitor position.

```
# Set Workspace to Specific Output
set $oLeft  "DVI-I-1"
set $oRight "DVI-D-0"

workspace $ws1 output $oLeft
workspace $ws2 output $oRight
workspace $ws3 output $oRight
workspace $ws4 output $oRight
workspace $ws5 output $oRight
```

I'm doing this because I have plans to run a remote desktop on the Left monitor
This "workspace" should remain unchanged by default, allowing me to add
workspace to the right without first switching to the right.

But I leave the higher number workspaces unbinded. This means I can change the
remote desktop workspace.

-----

Modify default programs

```
# start a terminal
bindsym $mod+Return exec tilix

# start dmenu (a program launcher)
bindsym $mod+e exec j4-dmenu-desktop
```

I change to my prefered terminal tilix.

I also installed j4-dmenu-desktop because this will utilize the installed
application .desktop files. The default dmenu will only run applications on
the path. This is problematic because the names aren't friendly and not all
applications add themselves to the path.